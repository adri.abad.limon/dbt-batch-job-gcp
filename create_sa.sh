# gcloud init to re-configure your default project
export PROJECT_ID="your-project-id"
export CICD_SA_NAME="your-cicd-service-account-name"
export DBT_SA_NAME="your-dbt-service-account-name"


# SA for publishing in Artifact registry
gcloud iam service-accounts create ${CICD_SA_NAME} \
    --description="CI/CD Service Account" \
    --display-name="${CICD_SA_NAME}"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${CICD_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/artifactregistry.admin"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${CICD_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/cloudbuild.builds.builder"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${CICD_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/iam.serviceAccountUser"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${CICD_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/storage.admin"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${CICD_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/viewer"


# SA for DBT run jobs
gcloud iam service-accounts create ${DBT_SA_NAME} \
    --description="DBT Batch SA" \
    --display-name="${DBT_SA_NAME}"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${DBT_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/bigquery.dataEditor"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${DBT_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/bigquery.jobUser"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${DBT_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/storage.objectViewer"
    
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${DBT_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/batch.agentReporter"
    
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member="serviceAccount:${DBT_SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
  --role="roles/logging.logWriter"