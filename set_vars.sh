#!/bin/bash

sed -i "s/ACCESS_TOKEN/$CI_REPO_TOKEN_DECODED/g" Dockerfile
sed -i "s#PROJECT_PATH#$CI_PROJECT_PATH#g" Dockerfile
sed -i "s#COMMIT_BRANCH#$CI_COMMIT_BRANCH#g" Dockerfile