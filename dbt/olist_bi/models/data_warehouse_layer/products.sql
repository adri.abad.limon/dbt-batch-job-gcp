
{{ config(materialized='table') }}

SELECT 
  orders.order_purchase_timestamp,  
  translation.product_category_name_english AS category_name,
  items.price,
  orders.customer_id,
  customers.customer_city,
  reviews.review_score
FROM {{ source('olist_store', 'orders') }} orders 
LEFT JOIN {{ source('olist_store', 'customers') }} customers USING (customer_id)
LEFT JOIN {{ source('olist_store', 'order_items') }}  items USING (order_id) 
LEFT JOIN {{ source('olist_store', 'products') }} products USING (product_id) 
LEFT JOIN {{ source('olist_store', 'product_category_name_translation') }} translation USING (product_category_name) 
LEFT JOIN {{ source('olist_store', 'order_reviews') }}  reviews USING (order_id)
WHERE order_status = 'shipped'

