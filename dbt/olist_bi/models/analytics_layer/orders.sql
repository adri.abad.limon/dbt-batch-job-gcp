
{{ config(materialized='table') }}

SELECT 
  orders.order_id,
  orders.customer_id,
  customers.customer_city,
  orders.order_purchase_timestamp,
  orders.order_estimated_delivery_date,
  products.product_category_name,
  items.price,
FROM {{ source('olist_store', 'orders') }} orders
LEFT JOIN {{ source('olist_store', 'customers') }} customers USING (customer_id)
LEFT JOIN {{ source('olist_store', 'order_items') }}  items USING (order_id) 
LEFT JOIN {{ source('olist_store', 'products') }}  products USING (product_id)  
WHERE order_status = 'shipped'