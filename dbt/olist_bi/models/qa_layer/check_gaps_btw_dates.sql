{{ config(materialized='table') }}

WITH dates AS (
  SELECT 
    DATE_ADD(DATE '2024-01-01', INTERVAL OFFSET DAY) AS day
  FROM UNNEST(GENERATE_ARRAY(0, DATE_DIFF(CURRENT_DATE(), DATE '2018-12-31', DAY))) AS OFFSET
),

orders AS (

SELECT 
  date(orders.order_purchase_timestamp) day,
  count(distinct order_id) count_orders
FROM {{ source('olist_store', 'orders') }} orders 
 group by 1

)

select 
    dates.day,
    orders.count_orders 
from dates
left join orders using (day)