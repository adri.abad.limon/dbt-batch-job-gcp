{{ config(materialized='table') }}

SELECT *
FROM {{ source('dbt_common', 'my_first_dbt_model') }}
WHERE id = 1