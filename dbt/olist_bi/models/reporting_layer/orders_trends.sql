
{{ config(materialized='table') }}

with orders_trends as (

    SELECT 
        date(orders.order_purchase_timestamp) date,
        products.product_category_name,
        sum(items.price) total,
    FROM {{ source('olist_store', 'orders') }}  orders 
    LEFT JOIN {{ source('olist_store', 'customers') }} customers USING (customer_id)
    LEFT JOIN {{ source('olist_store', 'order_items') }}  items USING (order_id) 
    LEFT JOIN {{ source('olist_store', 'products') }} products USING (product_id)  
    WHERE order_status = 'shipped'
    GROUP  BY 1,2
)

SELECT
    *
FROM orders_trends

