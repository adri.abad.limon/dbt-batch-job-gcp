FROM python:3.10

ENV PYTHONUNBUFFERED True

RUN apt-get update -y && \
  apt-get install --no-install-recommends -y -q \
  git libpq-dev python3-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY requirements.txt /
RUN pip install -U pip
RUN pip install --no-cache-dir -r requirements.txt

ENV HOME=/
WORKDIR ${HOME}

RUN apt-get update && apt-get install apt-transport-https ca-certificates -y
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN apt-get update && apt-get install google-cloud-sdk -y
RUN git clone --branch COMMIT_BRANCH https://ACCESS_TOKEN@gitlab.com/PROJECT_PATH.git

WORKDIR ${HOME}/dbt-batch-job-gcp/dbt/olist_bi

